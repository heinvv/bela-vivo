<section class="form">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <div class="col-sm-5 col-md-6">
                <h2 class="hsize1-lg">
                   <?php echo get_field('newsletter_title','option'); ?>
                </h2>
                 <div class="form__p p">
                    <?php echo get_field('newsletter_text','option'); ?>
                 </div>
            </div>
            <div class="col-sm-5 col-md-4">
                <?php echo get_field('newsletter_form','option'); ?>
            </div>
        </div>
    </div>
</section>