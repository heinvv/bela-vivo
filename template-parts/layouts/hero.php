<section class="hero-home" id="home"> 
    <div class="container-fluid">
         <div class="row">
            <div class="col-sm-12 col-lg-9">
                <h1 class="hsize1-lg">
                    <?php echo get_sub_field('title'); ?>
                </h1>
            </div>
        </div>
    </div>   
    <figure>
        <?php
            $imageID = get_sub_field('background_image');
            echo wp_get_attachment_image($imageID, 'full' );
        ?>                   
    </figure>
</section>