<?php
/* Template Name: Page Erens */
get_header();
?>

<section class="hero-home" id="home"> 
    <div class="container-fluid">
         <div class="row">
            <div class="col-sm-12 col-lg-9">
                <h1 class="hsize1-lg">
                    Wooninitiatief voor jongvolwassenen met een fysieke en/of geestelijke beperking
                </h1>
            </div>
        </div>
    </div>   
    <figure>
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/bella-vivo.png" alt="">                        
    </figure>
</section>


<!--col-grid 3 -->
<section class="col3-grid"> 
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4">
                    <figure>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/press-conference.png" alt="">                        
                    </figure>
                    <h2 class="hsize1-s">Waarom</h2>
                    <div class="col3-grid__p p">
                         <p>
                         Voor jong volwassenen met een fysieke en/of verstandelijke beperking is het niet 
                         vanzelfsprekend om op een ‘normale’ leeftijd het huis uit te gaan. Een beschermde 
                         woonomgeving, en tóch ook een onderdeel kunnen zijn van de ‘gewone’ maatschappij, 
                         is vanwege deze beperkingen van groot belang. Daarom is een aantal ouders dit 
                         wooninitiatief gestart, met ondersteuning van een bevriende en betrokken 
                         projectmanager.
                         </p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <figure>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/press-conference.png" alt="">                        
                    </figure>
                    <h2 class="hsize1-s">Onze visie</h2>
                    <div class="col3-grid__p p">
                    <p>
                    Het doel is om met dit wooninitiatief over 5 tot 7 jaar een veilige woonplek te realiseren, 
                    voor wellicht 8 tot 10 jong volwassenen. Het wooninitiatief moet ruimte bieden aan een aantal 
                    bewoners die deels hun eigen woonruimte hebben (badkamer, slaapkamer, evt. kitchenette) en een 
                    gedeelde woonkamer en recreatie-/buitenruimte. Geborgenheid en stimulans tot zelfstandigheid 
                    staan voorop, in de buurt van winkels en binnen een woonwijk van de BAR Gemeente. 
                    Er is 24-uurs hulp en begeleiding nodig evenals een gedeeltelijke verpleegkundige hulp. 
                    De regie over deze zorg zal door (vertegenwoordigers van) de bewoners op basis van PGB ingezet 
                    gaan worden.
                    </p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <figure>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/press-conference.png" alt="">                        
                    </figure>
                    <h2 class="hsize1-s">Meer weten?</h2>
                    <div class="col3-grid__p p"> 
                    <p>
                    Bela Vivo* is een stichting (in oprichting) die nog in de kinderschoenen staat. 
                    Gaandeweg zullen de plannen steeds realistischer worden. Met uiteindelijk … de 
                    daadwerkelijke realisatie van het initiatief.
                    </p>
                    <p>
                    <br>
                    Wilt u meer weten? Heeft u ideeën, om aan te sluiten of een bijdrage te leveren? 
                    Neem gerust contact op.
                    </p>
                    </div>
                </div>
            </div>
        </div>
</section>
<!--news -->
<section class="news">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5">
                 <h2 class="hsize1-lg news__mainh2">Laatste nieuws</h2>
                 <figure>
                 <img src="<?php echo get_template_directory_uri(); ?>/assets/images/bella-vivo.png" alt="Bella Vivo">                        
                 </figure>
                 <h3 class="hsize1-sm">Wij zijn een stichting!</h3>
                 <div class="news__p p">
                     <p>
                        Op 17 mei jl. waren we aanwezig bij Martin Tempelaar op Notariskantoor Groenveld en 
                        van Houdt, om de akte voor de stichting te ondertekenen.
                     </p>
                 </div>
            </div>
            <div class="col-md-5 offset-md-2">
                <figure>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/image.png" alt="">                        
                </figure>
                <h3 class="hsize1-sm">Even  voorstellen</h3>
                <div class="news__p p">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                        Lacus, fermentum amet faucibus sed id nisi lectus at.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<!--Text -->

<section class="text">
<figure class="text__figure">
         <img src="<?php echo get_template_directory_uri(); ?>/assets/images/bella-vivo.png" alt="Bella Vivo"> 
</figure>
    <div class="container-fluid">    
        <div class="row">
            <div class="col-sm-12 ">
                <div class="text__wrapper">
                <h1 class="hsize1-md">Wij zijn een stichting!</h1>
                    <div class="text__p p">
                        <p>
                            Op 17 mei jl. waren we aanwezig bij Martin Tempelaar op Notariskantoor Groenveld en 
                            van Houdt, om de akte voor de stichting te ondertekenen. Martin had aangeboden dat 
                            zijn notariskantoor wel een deel van de kosten voor hun rekening wilde nemen, als wij 
                            hierover dan wel een artikel in De Schakel wilde plaatsen. Daar zijn we natuurlijk al 
                            erg blij mee. Maar we vonden dat we hen nog wel extra konden bedanken — met een 
                            Dudok-appeltaart.
                        </p>                        
                        <p>
                            Stichting Bela-Vivo…. weer een belangrijke stap in het verwezenlijken van onze 
                            gezamenlijke droom
                        </p>                        
                        <p>
                            Het artikel kun je hier teruglezen.
                        </p>                        
                        <p>
                            Wilt u meer weten over onze plannen, neem dan gerust contact via 
                        <a href="#">info@bela-vivo.nl</a>.
                        </p>                        
                        <p>
                            Overigens, Bela Vivo staat voor ‘Mooi Leven’ in het Esperanto — een taal speciaal 
                            ontworpen om mensen uit verschillende ‘culturen’ met elkaar te kunnen verbinden.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="form">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <div class="col-sm-5 col-md-6">
                <h2 class="hsize1-lg">
                   Meld je aan voor onze nieuwsbrief
                </h2>
                 <div class="form__p p">
                     <p>
                     A urna etiam vel, diam non. Integer viverra nascetur mattis platea scelerisque etiam in elementum.
                     </p>
                 </div>
            </div>
            <div class="col-sm-5 col-md-4">
                 <?php echo do_shortcode(' [mc4wp_form id="11"] '); ?>
            </div>
        </div>
    </div>

</section>

<footer id="colophon" class="footer">
    <div class="container-fluid">      
       <div class="row">
           <div class="col">
                <div class="footer__logo d-flex justify-content-center">
                   <p>
                       stichting 
                       <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png" alt="Logo">
                       Bela-Vivo
                   </p>
                </div>
           </div>
       </div>
       <div class="row">
           <div class="col">
               <nav class="footer__nav">
                   <ul class="footer__ul ">
                       <li>
                           <a href="#">Home</a>
                       </li>
                       <li>
                           <a href="#">Onze Visie</a>
                       </li>
                       <li>
                           <a href="#">Contact</a>
                       </li>
                   </ul>
               </nav>
           </div>
       </div>
       <hr>
       <div class="row">
           <div class=" col-6">
               <div class="footer__p-copy-rights">
                    <p>© 2021 Bela-Vivo</p>
               </div>
           </div>
           <div class="col-6">
                   <div class="footer__social d-flex justify-content-end">
                        <a href="#"><i class="fab fa-youtube fa-xs"></i></a>
                        <a href="#"><i class="fab fa-youtube fa-xs"></i></a>
                        <a href="#"><i class="fab fa-youtube fa-xs"></i></a>
                        <a href="#"><i class="fab fa-youtube fa-xs"></i></a>
                    </div>
                </div>
            </div>
       </div>     
   </div> 
</footer><!-- #colophon -->
</div><!-- #page -->


<?php get_footer(); ?>