<section class="text">
    <figure class="text__figure">
        <?php 
            if( has_post_thumbnail() ){
                $imageID = get_post_thumbnail_id();
                echo wp_get_attachment_image( $imageID, 'full' );
                
            }
        ?> 
    </figure>
    <div class="container-fluid">    
        <div class="row">
            <div class="col-sm-12 ">
                <div class="text__wrapper">
                <h1 class="hsize1-md"><?php echo get_the_title(); ?></h1>
                    <div class="text__p p">
                        <?php echo get_sub_field('text');?>         
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>