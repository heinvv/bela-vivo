<section class="col3-grid"> 
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="col3-grid__title">
                    <h2 class="hsize48"><?php echo get_sub_field('section_title'); ?></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <?php
                // Check rows exists.
                if( have_rows('columns') ):

                    // Loop through rows.
                    while( have_rows('columns') ) : the_row();
                        ?>
                            <div class="col-lg-4">
                                <figure>
                                    <?php
                                        $imageID = get_sub_field('image');
                                        echo wp_get_attachment_image($imageID, 'large' );
                                    ?>
                                </figure>
                                <h2 class="hsize1-s"><?php echo get_sub_field('title'); ?></h2>
                                <div class="col3-grid__p p">
                                    <?php echo get_sub_field('text'); ?>
                                </div>
                            </div>
                        <?php
                    // End loop.
                    endwhile;

                // No value.
                else :
                    // Do something...
                endif;
            ?>
        </div>
    </div>
</section>