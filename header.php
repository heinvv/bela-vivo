<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package AM_Starter_Theme
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.12.0/css/all.css" integrity="sha384-ekOryaXPbeCpWQNxMwSWVvQ0+1VrStoPJq54shlYhR8HzQgig1v5fas6YgOqLoKz" crossorigin="anonymous">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<header class="header">
	<div class="header__wrapper">
		<div class="container-fluid">
			<nav class="navbar navbar-expand-md navbar-light"> 
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
					<div id="hamburger-menu"><span></span><span></span><span></span></div>
				</button>
				<a href="<?php echo site_url(); ?>" class="navbar-brand header__contact-btn--mobile img-mobile">
					<?php
						$imageID = get_field('logo','option');
						echo wp_get_attachment_image($imageID, 'thumbnail' );
					?>
				</a>
				<div class="collapse navbar-collapse justify-content-between" id="collapsibleNavbar">
					<?php
						wp_nav_menu(
							array(
								'menu' => 'Main Menu',
								'theme_location' => 'primary',
								'depth' => 2,
								'container-fluid' => false,
								'menu_class' => 'navbar-nav',
								'fallback_cb' => 'wp_page_menu',
								'walker' => new wp_bootstrap_navwalker()
							)
						);
					?>
					<a href="<?php echo site_url(); ?>" class="navbar-brand contact-btn">
					    <img class="navbar-brand--img-lg-screens" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png" alt="">
				    </a>
                    <div class="header__socialul">
						<ul class="navbar-nav">
							<?php
								// Check rows exists.
								if( have_rows('social_items','option') ):

									// Loop through rows.
									while( have_rows('social_items','option') ) : the_row();

										$link = get_sub_field('link');
										?>
											<li class="nave-item">
												<a href="<?php echo $link; ?>" target="_blank" class="nav-link"><?php echo get_sub_field('icon'); ?></a>
											</li>
										<?php 

									// End loop.
									endwhile;

								// No value.
								else :
									// Do something...
								endif;
							?>
            			</ul>
					</div>
				</div>
			</nav>			
		</div>
	</div>
</header>