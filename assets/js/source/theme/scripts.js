// Open menu on mobile with menu-toggle
document.addEventListener('click', () => {
    if (document.getElementById('site-navigation').classList.contains('toggled')) {
        document.body.classList.add('menu-open');
    } else {
        document.body.classList.remove('menu-open');
    }
});

document.addEventListener('keydown', (e) => {
    if(e.key === 'Escape') {
        document.body.classList.remove('menu-open');
        document.getElementById('site-navigation').classList.remove('toggled')
        document.getElementById('menu-toggle').setAttribute('aria-expanded', 'false');
    }
});


(function($) { 
    $(document).ready(function(){ 
  
      
        alert("test");
     
    
      // hide empty paragraphs
      $('p:empty').remove();
  
      // remove special characters van tel: href link
      $('a[href^="tel:"]').attr('href', function(_,v){
        return v.replace(/\(0\)|\s+/g,'');
      });  
  
    });
  })(jQuery);