<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package AM_Starter_Theme
 */

?>

	<footer id="colophon" class="footer">
		<div class="container-fluid">      
			<div class="row">
				<div class="col">
						<div class="footer__logo d-flex justify-content-center">
						<p>
							stichting 
							<?php
								$imageID = get_field('logo','option');
								echo wp_get_attachment_image($imageID, 'thumbnail' );
							?>
							Bela-Vivo
						</p>
						</div>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<nav class="footer__nav">
						<?php
							wp_nav_menu(
								array(
									'menu' => 'Main Menu',
									'theme_location' => 'primary',
									'depth' => 2,
									'container-fluid' => false,
									'menu_class' => 'navbar-nav',
									'fallback_cb' => 'wp_page_menu',
									'walker' => new wp_bootstrap_navwalker()
								)
							);
						?>
					</nav>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class=" col-6">
					<div class="footer__p-copy-rights">
							<p>© <?php echo date('Y'); ?> <?php echo get_field('footer_copyright','option'); ?></p>
					</div>
				</div>
				<div class="col-6">
						<div class="footer__social d-flex justify-content-end">
							<?php
								// Check rows exists.
								if( have_rows('social_items','option') ):

									// Loop through rows.
									while( have_rows('social_items','option') ) : the_row();

										$link = get_sub_field('link');
										?>
											<a href="<?php echo $link; ?>" target="_blank"><?php echo get_sub_field('icon'); ?></a>
										<?php 

									// End loop.
									endwhile;

								// No value.
								else :
									// Do something...
								endif;
							?>
						</div>
					</div>
			</div>     
		</div> 
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

<script id="__bs_script__">//<![CDATA[
    document.write("<script async src='http://HOST:3000/browser-sync/browser-sync-client.js?v=2.26.14'><\/script>".replace("HOST", location.hostname));
//]]></script>
</body>
</html>
