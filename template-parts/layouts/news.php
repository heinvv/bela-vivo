<section class="news">
    <div class="container-fluid">
        <div class="row">
            <?php
                // args
                $args = array(
                    'posts_per_page'	=> 2,
                    'post_type'		    => 'post',
                );

                // query
                $the_query = new WP_Query( $args );
                $count = 0;
                ?>
                <?php if( $the_query->have_posts() ): ?>
                    <?php while( $the_query->have_posts() ) : $the_query->the_post(); 
                        if($count == 0 ){
                            $offset = '';
                        } else {
                            $offset = 'offset-md-2';
                        }
                        ?>
                        <div class="col-md-5 <?php echo $offset; ?>">
                            <?php if($count == 0 ){ ?>
                                <h2 class="hsize1-lg news__mainh2">Laatste nieuws</h2>
                            <?php } ?>
                            <figure>
                                <a href="<?php echo get_the_permalink(); ?>">
                                    <?php 
                                        if( has_post_thumbnail() ){
                                            $imageID = get_post_thumbnail_id();
                                            echo wp_get_attachment_image( $imageID, 'large' );
                                        }
                                    ?>            
                                </a>
                            </figure>
                            <h3 class="hsize1-sm"><?php echo get_the_title(); ?></h3>
                            <div class="news__p p-18">
                                <p>
                                    <?php
                                        $intro = strip_tags( get_the_content() ); 
                                        $intro = substr($intro,0,175);
                                        $last_space_position = strrpos($intro, ' ');
                                        $intro = substr($intro, 0, $last_space_position);
                                        echo $intro. ' ...'; 
                                    ?>
                                    <a href="<?php echo get_the_permalink(); ?>" class="news__permalink">Lees meer</a>
                                </p>
                            </div>
                        </div>
                    <?php
                        $count++; 
                        endwhile; ?>
                <?php endif; ?>

                <?php wp_reset_query();	
            ?>
        </div>
    </div>
</section>