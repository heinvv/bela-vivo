// add AOS animation   
// Documentation: https://michalsnik.github.io/aos/
// if(jQuery(window).innerWidth() < 768 ) { 
//   AOS.init({
//     disable: true              
//   })
// }
// if(jQuery(window).innerWidth() >= 768 ) {   
//   AOS.init({ 
//     easing: 'linear',        
//     duration: 400 
//   })
// } 



(function ($) {
  $(document).ready(function () {

        //Header sticky navbar
        $(window).scroll(function(){
          var header = $('.header').height();
          if ($(window).scrollTop() >= header) {
              $('header').addClass('header__fixed-header');
              
          }
          else {
              $('header').removeClass('header__fixed-header');
             
          }
      });
      // Add active and remove active class when hamburger is clicked on
      $('.navbar-toggler').on('click', function () {
        if ($(".navbar-toggler").hasClass('active')) {
          $(".navbar-toggler").removeClass('active');
        } else {
          $(".navbar-toggler").addClass('active');
        }
      });
  
      // Add active and remove active class when hamburger is clicked on 
      $('.navbar-toggler').on('click', function () {
        if ($(".header__wrapper").hasClass('active')) {
          $(".header__wrapper").removeClass('active');
        } else {
          $(".header__wrapper").addClass('active');
        }
      });


    $(window).scroll(function () {
        var header = $('.header').height();       
        // var heroHome = $('.hero-home').height();
        // var allElementsHeight = header + heroHome;

      if ($(this).scrollTop() > header) {
          $('.header').addClass('scroll-active');
        } else {
          $('.header').removeClass('scroll-active');
        }
    });
    
    // hide empty paragraphs
    $('p:empty').remove();

    // remove special characters van tel: href link
    $('a[href^="tel:"]').attr('href', function(_,v){
      return v.replace(/\(0\)|\s+/g,'');
    });  


  
    
  });
})(jQuery);