<?php
/**
 * am Starter Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package AM_Starter_Theme
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'am_custom_setup' ) ) {
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function am_custom_setup() {

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		add_image_size( 'column', 600, 600, true );
        add_image_size( 'columnsmall', 300, 300, true );
		add_image_size( 'bg', 2400, 1200, true );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'primary-menu' => esc_html__( 'Primary', 'am_custom' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			[
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
            ]
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
}
add_action( 'after_setup_theme', 'am_custom_setup' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function am_custom_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'am_custom' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'am_custom' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'am_custom_widgets_init' );

/**
 * Register Custom Navigation Walker
 */
function register_navwalker(){
	require_once get_template_directory() . '/inc/class-wp-bootstrap-navwalker.php';
}
add_action( 'after_setup_theme', 'register_navwalker' );


/**
 * Enqueue scripts and styles.
 */
function am_custom_scripts() { 
	wp_enqueue_style( 'am_custom-style', get_stylesheet_uri(), array(), _S_VERSION );

    // wp_enqueue_script( 'am_custom-app', get_template_directory_uri() . '/assets/js/app.js', array(), _S_VERSION, true );
    wp_enqueue_script( 'am_custom-jquery', get_template_directory_uri() . '/assets/js/jquery.min.js', array(), _S_VERSION, true );
    wp_enqueue_script( 'am_custom-bootstrap', get_template_directory_uri() . '/assets/js/vendor/bootstrap.bundle.min.js', array(), _S_VERSION, true );
    // wp_enqueue_script( 'am_custom-navigation', get_template_directory_uri() . '/assets/js/navigation.js', array(), _S_VERSION, true );
    // wp_enqueue_script( 'am_custom-bundle', get_template_directory_uri() . '/assets/js/bundle.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'am_custom_scripts' );

foreach (glob(get_template_directory() . "/inc/*.php") as $filename) {
    require $filename;
}

/**
 * SVG files
 */
add_filter('upload_mimes', function($file_types){
    $new_filetypes = array();
    $new_filetypes['svg'] = 'image/svg+xml';
    $file_types = array_merge($file_types, $new_filetypes );
    return $file_types;
});

//.....................................................................................

function am_theme_styles() {
	$style_ver = filemtime( get_stylesheet_directory() . '/assets/scss/plugins/swiper-bundle.min.css' );
	wp_enqueue_style( 'swiperjs', get_stylesheet_directory_uri() . '/assets/scss/plugins/swiper-bundle.min.css', '', $style_ver );
}
add_action( 'wp_print_styles', 'am_theme_styles' );

// Enqueue JavaScript in our theme
function am_theme_scripts() {
	// wp_deregister_script( 'jquery' );
	$style_ver = filemtime( get_template_directory() . '/assets/js/all.min.js' );
	wp_enqueue_script( 'main-js', get_template_directory_uri() . '/assets/js/all.min.js', '', $style_ver, true);

}
add_action( 'wp_enqueue_scripts', 'am_theme_scripts' );

// // Create a custom taxonomy FAQ
// add_action( 'init', 'create_subjects_hierarchical_taxonomy', 0 );
// function create_subjects_hierarchical_taxonomy() {
// 	$labels = array(
// 		'name' => _x( 'Faq Categories', 'taxonomy general name' ),
// 		'singular_name' => _x( 'Faq Category', 'taxonomy singular name' ),
// 		'search_items' =>  __( 'Search Faq Categories' ),
// 		'all_items' => __( 'All Faq Categories' ),
// 		'parent_item' => __( 'Parent Faq Category' ),
// 		'parent_item_colon' => __( 'Parent Faq Category:' ),
// 		'edit_item' => __( 'Edit Faq Category' ), 
// 		'update_item' => __( 'Update Faq Category' ),
// 		'add_new_item' => __( 'Add New Faq Category' ),
// 		'new_item_name' => __( 'New Faq Category Name' ),
// 		'menu_name' => __( 'Faq Categories' ),
// 	);    
//   	register_taxonomy('faq-category',
// 		array('faq'), 
// 		array(
// 		'hierarchical' => true,
// 		'labels' => $labels,
// 		'show_ui' => true,
// 		'show_in_rest' => true,
// 		'show_admin_column' => true,
// 		'query_var' => true,
// 		'rewrite' => array( 'slug' => 'faq-category' ),
//     )
//   );
// }












// saving acf fields
add_filter('acf/settings/save_json', 'my_acf_json_save_point');
 
function my_acf_json_save_point( $path ) {
    
    // update path
    $path = get_stylesheet_directory() . '/acf-fields';
    
    // return
    return $path;
    
}
add_filter('acf/settings/load_json', 'my_acf_json_load_point');

function my_acf_json_load_point( $paths ) {
    
    // remove original path (optional)
    unset($paths[0]);
    
    // append path
    $paths[] = get_stylesheet_directory() . '/acf-fields';
    
    
    // return
    return $paths;
    
}

//Register your Google API key,
function my_acf_google_map_api( $api ){
    $api['key'] = 'AIzaSyBOg7X_LpCEJKE1tDINhBkcp3WRG-Jlpe8';
    return $api;
}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

// load gravity js in footer and set submission anchor
add_filter( 'gform_init_scripts_footer', '__return_true' );
add_filter( 'gform_confirmation_anchor', '__return_true' );