<?php
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page([
        'page_title' => __('Site Options'),
        'menu_title' => __('Site Options')
    ]);
}