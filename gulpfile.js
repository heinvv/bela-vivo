const gulp = require('gulp');
const sass = require('gulp-sass');
const concat= require('gulp-concat');
const terser= require('gulp-terser');
const browserSync = require('browser-sync').create();
var rename = require('gulp-rename');
const {src, dest, parallel, series}   = require('gulp');
//var localhost_link= "https://localhost/adwinpeeks";
var localhost_link= "https://localhost/wordpress/erens/";


//.....................................   == compile scss into css style and style.min == ..........................
function style() { 
    var config= {};
    config.outputStyle= 'compressed';
	return gulp.src('./assets/scss/style.scss') 
    .pipe(sass()) 
    .pipe(gulp.dest('./'))  
    .pipe(sass(config)) 
    .pipe(rename("style.css"))
    .pipe(gulp.dest('./'))
    .pipe(browserSync.stream());   
} 

//.....................................   == js file  == ..........................
function script() {
    return gulp.src([
        // './assets/js/plugins-js/jquery.min.js',
        './assets/js/plugins-js/popper.min.js',
        './assets/js/plugins-js/aos.js',
        './assets/js/plugins-js/swiper.js',
        // './assets/js/plugins-js/fontawesome-markers.min.js',
        // './node_modules/bootstrap/dist/js/bootstrap.js',
        './assets/js/local-js/**/*.js'])
    .pipe(concat('all.js'))
    .pipe(gulp.dest('./assets/js'))
    .pipe(terser())
    .pipe(rename("all.min.js"))
    .pipe(gulp.dest('./assets/js'))
    .pipe(browserSync.stream());   
}


// .....................................   == Live Server == ..........................
function watch() {
    browserSync.init({ 
        sever: {
            baseDir: './'
        }
    },{ 
        proxy: localhost_link
    });
    gulp.watch('./assets/scss/**/*.scss', style);
    gulp.watch('./*.php').on('change', browserSync.reload);
    // gulp.watch('./assets/js/**/*.js').on('change', browserSync.reload);
    gulp.watch(['./assets/js/plugins-js/**/*.js','./assets/js/local-js/**/*.js'], script); 
}



exports.style = style; 
exports.script = script;
exports.watch = watch;   
exports.default = watch;   



