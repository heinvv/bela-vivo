<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package AM_Starter_Theme
 */

if( have_rows('page_builder') ):
    while ( have_rows('page_builder') ) : the_row();
        if( get_row_layout() == 'hero' )
            include(get_template_directory().'/template-parts/layouts/hero.php');
        elseif( get_row_layout() == 'hero_sub' )
            include(get_template_directory().'/template-parts/layouts/hero-sub.php');
        elseif( get_row_layout() == 'content' )
            include(get_template_directory().'/template-parts/layouts/content.php');
        elseif( get_row_layout() == 'col_grid' )
            include(get_template_directory().'/template-parts/layouts/col3-grid.php');
        elseif( get_row_layout() == 'latest_news' )
            include(get_template_directory().'/template-parts/layouts/news.php'); 
    endwhile;
endif;
include(get_template_directory().'/template-parts/layouts/newsletter.php'); 

// ?>

