<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package AM_Starter_Theme
 */

get_header();
?>
<main id="primary" class="site-main">
	<section class="text">
        <figure class="text__figure">
            <?php 
                if( has_post_thumbnail() ){
                    $imageID = get_post_thumbnail_id();
                    echo wp_get_attachment_image( $imageID, 'full' );
                    
                }
            ?> 
		</figure>
        <div class="container-fluid">    
            <div class="row">
                <div class="col-sm-12 ">
                    <div class="text__wrapper">
                    <h1 class="hsize1-md"><?php echo get_the_title(); ?></h1>
                        <div class="text__p p">
                            <p>
							    <?php echo the_content();?>
                            </p>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php 
        include(get_template_directory().'/template-parts/layouts/newsletter.php'); 
    ?>
</main><!-- #main -->

<?php
// get_sidebar();
get_footer();
