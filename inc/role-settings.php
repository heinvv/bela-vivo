<?php
/*
 * Removes from admin menu
 */
add_action( 'admin_menu', function() {
    remove_menu_page( 'edit-comments.php' );

    if (current_user_can('manage_options') === false) {
        remove_menu_page( 'tools.php' );
    }
});

/*
 * Removes from post and pages
 */
add_action('init', function() {
    remove_post_type_support( 'post', 'comments' );
    remove_post_type_support( 'page', 'comments' );
}, 100);

/*
 * Removes from admin bar
 */
add_action( 'wp_before_admin_bar_render', function() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('comments');
});


add_action('admin_head', function() {
    if (current_user_can('manage_options') === true) {
        return;
    }

    $role_object = get_role( 'editor' );
    $role_object->add_cap( 'edit_theme_options' );

    global $submenu;
    unset($submenu['themes.php'][5]);
    unset($submenu['themes.php'][6]);
    unset($submenu['themes.php'][7]);
    unset($submenu['themes.php'][15]);
    unset($submenu['themes.php'][20]);
    unset($submenu['themes.php'][21]);
    unset($submenu['themes.php'][22]);
});

/*
 * Allow users to edit privacy page
 * */
add_action('map_meta_cap', function($caps, $cap, $user_id, $args) {
    if (!is_user_logged_in()) {
        return $caps;
    }

    $user_meta = get_userdata($user_id);

    if (array_intersect(['editor', 'administrator'], $user_meta->roles) && 'manage_privacy_options' === $cap) {
        $manage_name = is_multisite() ? 'manage_network' : 'manage_options';
        $caps = array_diff($caps, [ $manage_name ]);
    }

    return $caps;
}, 1, 4);